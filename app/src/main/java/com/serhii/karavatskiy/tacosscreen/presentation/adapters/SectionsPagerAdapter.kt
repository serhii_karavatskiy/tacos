package com.serhii.karavatskiy.tacosscreen.presentation.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.serhii.karavatskiy.tacosscreen.R
import com.serhii.karavatskiy.tacosscreen.presentation.dishes.DishesFragment
import com.serhii.karavatskiy.tacosscreen.presentation.ustils.ARGS_TITLE
import com.serhii.karavatskiy.tacosscreen.presentation.ustils.withArguments

class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> {
                DishesFragment.newInstance()
                    .withArguments(Pair(ARGS_TITLE, context.resources.getString(TAB_TITLES[position])))
            }
            1 -> {
                DishesFragment.newInstance()
                    .withArguments(Pair(ARGS_TITLE, context.resources.getString(TAB_TITLES[position])))
            }
            2 -> {
                DishesFragment.newInstance()
                    .withArguments(Pair(ARGS_TITLE, context.resources.getString(TAB_TITLES[position])))
            }
            3 -> {
                DishesFragment.newInstance()
                    .withArguments(Pair(ARGS_TITLE, context.resources.getString(TAB_TITLES[position])))
            }
            4 -> {
                DishesFragment.newInstance()
                    .withArguments(Pair(ARGS_TITLE, context.resources.getString(TAB_TITLES[position])))
            }
            else -> {
                DishesFragment.newInstance()
                    .withArguments(Pair(ARGS_TITLE, context.resources.getString(TAB_TITLES[position])))
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return TAB_TITLES.size
    }
}

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2,
    R.string.tab_text_3,
    R.string.tab_text_4,
    R.string.tab_text_5,
    R.string.tab_text_6
)

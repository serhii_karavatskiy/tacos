package com.serhii.karavatskiy.tacosscreen.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.serhii.karavatskiy.tacosscreen.data.model.ItemDish
import com.serhii.karavatskiy.tacosscreen.databinding.ItemDishBinding

class DishesAdapter : RecyclerView.Adapter<DishViewHolder>() {
    private lateinit var binding: ItemDishBinding

    private val dishes = mutableListOf<ItemDish>()

    fun addDishes(list: List<ItemDish>) {
        dishes.clear()
        dishes.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DishViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemDishBinding.inflate(inflater, parent, false)

        return DishViewHolder(binding)
    }

    override fun getItemCount(): Int = dishes.size

    override fun onBindViewHolder(holder: DishViewHolder, position: Int) {
        holder.bind?.item = dishes[position]
        holder.bind?.executePendingBindings()
    }
}

class DishViewHolder(binding: ItemDishBinding) : RecyclerView.ViewHolder(binding.root) {
    val bind: ItemDishBinding? = DataBindingUtil.bind(binding.root)
}
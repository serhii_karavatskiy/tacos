package com.serhii.karavatskiy.tacosscreen.presentation

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.serhii.karavatskiy.tacosscreen.R
import com.serhii.karavatskiy.tacosscreen.presentation.adapters.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupWindow()
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun setupWindow() {
        this.window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        setupView()
    }

    private fun setupView() {
        vp_items_fragment.adapter = SectionsPagerAdapter(this, supportFragmentManager)
        tabs.setupWithViewPager(vp_items_fragment)
    }
}

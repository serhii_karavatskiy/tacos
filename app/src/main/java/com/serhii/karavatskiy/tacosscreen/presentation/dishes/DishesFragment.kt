package com.serhii.karavatskiy.tacosscreen.presentation.dishes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.serhii.karavatskiy.tacosscreen.R
import com.serhii.karavatskiy.tacosscreen.presentation.PhotosViewModel
import com.serhii.karavatskiy.tacosscreen.presentation.adapters.DishesAdapter
import com.serhii.karavatskiy.tacosscreen.presentation.ustils.ARGS_TITLE
import com.serhii.karavatskiy.tacosscreen.presentation.ustils.hide
import com.serhii.karavatskiy.tacosscreen.presentation.ustils.observe
import com.serhii.karavatskiy.tacosscreen.presentation.ustils.show
import kotlinx.android.synthetic.main.fragment_dishes.*

class DishesFragment : Fragment() {

    private lateinit var progress: View

    private lateinit var viewModel: PhotosViewModel
    private val adapter by lazy { DishesAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_dishes, container, false
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PhotosViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let { progress = it.findViewById(R.id.rl_progress) }
        observe(viewModel.getDishesData()) { items ->
            progress.hide()
            rv_dishes.adapter = adapter
            rv_dishes.layoutManager = LinearLayoutManager(context)
            adapter.addDishes(items)
        }
        arguments?.let {
            it.getString(ARGS_TITLE)?.let { title ->
                viewModel.requestPhotos(title)
                progress.show()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = DishesFragment()
    }
}
package com.serhii.karavatskiy.tacosscreen.presentation.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object BindingsAdapter {
    @BindingAdapter("imageFromUrl")
    @JvmStatic
    fun imageFromUrl(imageView: ImageView, url: String?) {
        if (!url.isNullOrEmpty()) {
            Glide.with(imageView.context)
                .asBitmap()
                .placeholder(android.R.drawable.ic_menu_camera)
                .load(url)
                .fitCenter()
                .into(imageView)
        }
    }

}
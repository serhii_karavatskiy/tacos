package com.serhii.karavatskiy.tacosscreen.data.repository

import com.serhii.karavatskiy.tacosscreen.data.api.ImageApi
import com.serhii.karavatskiy.tacosscreen.data.model.PageImagesUrl
import com.serhii.karavatskiy.tacosscreen.data.utils.BASE_URL
import com.serhii.karavatskiy.tacosscreen.data.utils.PageAdapter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class NetworkRepository {

    private var loggingInterceptor =
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BASIC }

    private var retrofit: Retrofit =
        Retrofit.Builder().baseUrl(BASE_URL)
            .client(OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build())
            .addConverterFactory(PageAdapter.FACTORY)
            .build()


    private fun getApi(): ImageApi {
        return retrofit.create(ImageApi::class.java)
    }

    fun getImagesUrl(searchingFor: String, imagesUrl: (List<String>) -> Unit) {
        getApi().getBurgerImages(searchingFor).enqueue(object : Callback<PageImagesUrl> {
            override fun onFailure(call: Call<PageImagesUrl>, t: Throwable) {
                imagesUrl.invoke(emptyList())
            }

            override fun onResponse(call: Call<PageImagesUrl>, response: Response<PageImagesUrl>) {
                if (response.isSuccessful) {
                    response.body()?.let { imagesUrl.invoke(it.imageList) }
                }
            }

        })
    }

    companion object {
        private var instance: NetworkRepository? = null
        fun getInstance(): NetworkRepository {
            if (instance == null) {
                instance = NetworkRepository()
            }
            return instance as NetworkRepository

        }
    }
}
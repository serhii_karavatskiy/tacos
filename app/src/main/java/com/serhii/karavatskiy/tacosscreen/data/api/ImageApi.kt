package com.serhii.karavatskiy.tacosscreen.data.api

import com.serhii.karavatskiy.tacosscreen.data.model.PageImagesUrl
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ImageApi {
    @GET("search")
    fun getBurgerImages(@Query("q") searchFor: String): Call<PageImagesUrl>
}
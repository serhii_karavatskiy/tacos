package com.serhii.karavatskiy.tacosscreen.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.serhii.karavatskiy.tacosscreen.data.model.ItemDish
import com.serhii.karavatskiy.tacosscreen.data.repository.NetworkRepository
import com.serhii.karavatskiy.tacosscreen.presentation.ustils.HTTPS
import kotlin.random.Random

class PhotosViewModel : ViewModel() {

    private val dishesData by lazy { MutableLiveData<List<ItemDish>>() }

    fun getDishesData(): LiveData<List<ItemDish>> = dishesData

    fun requestPhotos(searchingForDish: String) {
        NetworkRepository.getInstance().getImagesUrl(searchingForDish) { listUrl ->
            dishesData.postValue(listUrl.filter { it.contains(HTTPS) }.map {
                ItemDish(it)
            })
        }
    }

}
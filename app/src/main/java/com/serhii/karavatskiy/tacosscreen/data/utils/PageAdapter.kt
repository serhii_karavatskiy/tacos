package com.serhii.karavatskiy.tacosscreen.data.utils

import com.serhii.karavatskiy.tacosscreen.data.model.PageImagesUrl
import okhttp3.ResponseBody
import org.jsoup.Jsoup
import retrofit2.Converter
import retrofit2.Retrofit
import java.io.IOException
import java.lang.reflect.Type

class PageAdapter : Converter<ResponseBody, PageImagesUrl> {

    @Throws(IOException::class)
    override fun convert(responseBody: ResponseBody): PageImagesUrl {
        val document = Jsoup.parse(responseBody.string())
        val value = document.getElementById(SEARCH_CANVAS)
        val other = value.getElementsByClass(SEARCH_THUMB)
        return PageImagesUrl(other.map { it.attributes().get(SEARCH_HREF) })
    }

    companion object {
        val FACTORY: Converter.Factory = object : Converter.Factory() {
            override fun responseBodyConverter(
                type: Type,
                annotations: Array<Annotation>,
                retrofit: Retrofit
            ): Converter<ResponseBody, *>? {
                return if (type === PageImagesUrl::class.java) PageAdapter() else null
            }
        }
    }
}


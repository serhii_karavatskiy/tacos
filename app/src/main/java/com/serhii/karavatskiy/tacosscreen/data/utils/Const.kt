package com.serhii.karavatskiy.tacosscreen.data.utils

const val BASE_URL = "https://bing.com/images/"

const val SEARCH_CANVAS = "canvas"
const val SEARCH_THUMB = "thumb"
const val SEARCH_HREF = "href"